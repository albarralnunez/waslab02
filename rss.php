<?php

$URI = 'http://localhost:8080/waslab02/wall.php';
$resp = file_get_contents($URI);
$tweet = new SimpleXMLElement($resp);

echo '<?xml version="1.0" encoding="UTF-8" ?>';
echo '<rss version="2.0">';
echo '<channel>';
echo '<title>WALL OF TWEETS</title>';
echo '<link>http://localhost:8080/waslab02/wall.php</link>';
echo '<description>WALL OF TWEETS</description>';

foreach ($tweet as $tweet) {
	echo '<item>';
	echo '<title>', $tweet->text  ,'</title>';
	echo '<link> http://localhost:8080/waslab02/wall.php#', $tweet['id'] ,'</link>';
	echo '<pubDate>', $tweet->time ,'</pubDate>';
	echo '<description>',  $tweet['id'], ', ' , $tweet->author, ', ', $tweet->numlikes ,'</description>';
	echo '</item>';	
}

echo '</channel>';
echo '</rss>';

?>
